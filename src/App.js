import React, {useState} from 'react';

import './App.css';

const SAMPLE_RATINGS = `
TOURNEY Princeton Go Club Rated Games, Princeton, NJ, June 15-30, 2004
        start=6/15/2004
        finish=6/30/2004
        rules=AGA

PLAYERS
 489 Matthews, Paul 4k
3199 Mott, Rick     4k

GAMES
489 3199 b 0 7
3199 489 w 0 7
`.trim();

function validate(raw) {
    const messages = [];
    const payload = {};

    const rawByLines = (
        raw.trim()
        .split('\n')
        .filter(l => !l.trim().startsWith('#'))
    );

    if (!rawByLines[0].startsWith('TOURNEY ')) {
        messages.push(['error', 'Input must begin with "TOURNEY "']);
        return [messages, null];
    }

    payload.description = rawByLines.shift().substr('TOURNEY '.length);

    while (
        rawByLines.length > 0
        && !rawByLines[0].startsWith('PLAYERS')
        && !rawByLines[0].startsWith('GAMES')
    ) {
        const line = rawByLines.shift();
        if (line.trim() === '') {
            continue;
        }

        if (line.split('=').length !== 2) {
            messages.push(['error', 'Invalid tournament metadata']);
            return [messages, null];
        }

        const [key, value] = line.split('=').map(s => s.trim());
        if (!['start', 'finish', 'rules'].includes(key)) {
            messages.push(['warning', 'Unused metadata key.']);
            continue;
        }

        if (payload[key]) {
            messages.push(['warning', `Duplicate metadata for ${key}`]);
        }

        payload[key] = value;
    }

    if (rawByLines.length === 0) {
        messages.push(['warning', 'No games recorded']);

        return [messages, payload];
    }

    if (rawByLines[0].startsWith('GAMES')) {
        messages.push(['warning', 'Missing PLAYERS section']);
    }

    rawByLines.shift();
    payload.players = [];

    while (rawByLines.length > 0 && !rawByLines[0].startsWith('GAMES')) {
        const line = rawByLines.shift().trim();
        if (line.trim() === '') {
            continue;
        }

        const tokenized = line.split(/\s/).filter(s => s.trim());
        if (tokenized.length < 3) {
            messages.push(['error', 'Invalid player entry']);
            return [messages, payload];
        }

        const player = {};
        player.id = tokenized[0];
        player.rank = tokenized.pop();
        player.name = line.substring(player.id.length, line.length - player.rank.length).trim();

        payload.players.push(player);
    }

    if (rawByLines.length === 0) {
        messages.push(['warning', 'No games recorded']);

        return [messages, payload];
    }

    rawByLines.shift();
    payload.games = [];

    while(rawByLines.length > 0) {
        const line = rawByLines.shift().trim();
        if (line.trim() === '') {
            continue;
        }

        const tokenized = (
            line.split(' ')
            .map(s => s.trim())
            .filter(s => s !== '')
        );

        if (tokenized.length !== 5) {
            messages.push(['error', 'Invalid game format']);
            return [messages, payload];
        }

        const [white, black, winner, handicap, komi] = tokenized;
        payload.games.push({white, black, winner, handicap, komi});
    }

    if (payload.games.length === 0) {
        messages.push(['warning', 'No games recorded']);
    }

    return [messages, payload];
}

function App() {
    const [rawRatings, setRawRatings] = useState(SAMPLE_RATINGS);

    const [messages, ratings] = validate(rawRatings);

    return <div className="root">
        <textarea wrap="off" value={rawRatings} onChange={event => setRawRatings(event.target.value)}/>
        <div className="report">
            {messages.map(([className, message], i) =>
                <div key={i} className={className}>{message}</div>
            )}
            <pre>{ratings && JSON.stringify(ratings, null, 2)}</pre>
        </div>
    </div>;
}

export default App;
